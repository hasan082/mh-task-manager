import 'package:flutter/material.dart';
import 'package:mh_todo/pages/todo_screen.dart';

// TODO: 1. Complete the bottom sheet adding Another filed
// TODO: 2. Enum file creation for G\Good Morning
// TODO: 3 Code Optimized Reusable widget making
// TODO: 4. Connect to the database
// TODO: 5 Create mvc model
// TODO: 6 Edit and delete Function activate
// TODO: Design improvement





void main() {
  runApp(const MhToDo());
}

class MhToDo extends StatelessWidget {
  const MhToDo({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'MH Task Manager',
      theme: ThemeData(
        primarySwatch: Colors.amber,
      ),
      home: const ToDoSCreen(),
    );
  }
}

