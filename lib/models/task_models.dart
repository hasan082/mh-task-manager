class ToDo {
  String title, description;
  bool isDone;

  ToDo(this.title, this.description, this.isDone);
}