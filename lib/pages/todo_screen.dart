import 'package:flutter/material.dart';

class ToDoSCreen extends StatefulWidget {
  const ToDoSCreen({Key? key}) : super(key: key);

  @override
  State<ToDoSCreen> createState() => _ToDoSCreenState();
}

class _ToDoSCreenState extends State<ToDoSCreen> {
  List<ToDo> todos = [];
  late TextEditingController _titleTextCtrl;

  DateTime now = DateTime.now();


  @override
  void initState() {
    super.initState();
    _titleTextCtrl = TextEditingController();
    print(now.toString().substring(0,10));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("MH Task Manager"),
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 15),
        children: [
          const Text(
            "Hi, Hasan, Good Morning",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
          ),
          ListView.separated(
            shrinkWrap: true,
            physics: const ScrollPhysics(),
            itemCount: todos.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                leading: const Icon(Icons.check),
                title: Text(
                  todos[index].title.toUpperCase(),
                  style: const TextStyle(
                      fontWeight: FontWeight.w500, fontSize: 18, ),maxLines: 1,
                ),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 6,),
                    Text(
                      todos[index].description,
                      style: const TextStyle(fontSize: 16), maxLines: 2,
                    ),
                    const SizedBox(height: 6,),
                    Text(now.toString().substring(0, 10))
                  ],
                ),
                trailing: PopupMenuButton<String>(
                  constraints: const BoxConstraints(maxWidth: 90),
                  // padding: EdgeInsets.zero,
                  itemBuilder: (context) {
                    return [
                      const PopupMenuItem<String>(
                        value: "edit",
                        child: Icon(Icons.edit),
                      ),
                      const PopupMenuItem(
                        value: 'delete',
                        child: Icon(Icons.delete),
                      )
                    ];
                  },
                ),
              );
            }, separatorBuilder: (BuildContext context, int index) {
              return const Divider(height: 3,);
          },
          )
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            setState(() {
              todos.add(ToDo('title', 'description', false));
              showModalBottomSheet(
                  context: context,
                  builder: (context) {
                    return Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        children: [
                          const Text("Add Task", textAlign: TextAlign.center, style: TextStyle(fontSize: 25),),
                          const SizedBox(height: 20,),
                          TextField(
                            controller: _titleTextCtrl,
                            decoration: const InputDecoration(
                                labelText: "Task Title",
                              border: OutlineInputBorder(),
                            ),

                          ),
                        ],
                      ),
                    );
                  });
            });
          },
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          label: const Text('Add Task')),
    );
  }
}

class ToDo {
  String title, description;
  bool isDone;

  ToDo(this.title, this.description, this.isDone);
}
